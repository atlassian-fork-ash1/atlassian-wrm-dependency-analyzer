export {
    IEntryResourceDependencies as IEntryDependenciesDescriptor,
} from 'atlassian-wrm-external-dependencies-size-calculator/src/dependency-usage-generator';

export type WebresourceKey = string;
export type EntryPointName = string;

interface ISizesDescriptor {
    [name: string]: {
        css: number;
        js: number;
        total: number;
    };
}

interface IExternalSizeDescriptor {
    css: number;
    js: number;
    total: number;
}

export interface IModuleDescriptor {
    location: string;
    size: number;
    webresourceKey: WebresourceKey;
}

export interface IChunkDescriptor {
    webresourceKey: WebresourceKey;
    size: number;
    modules: Array<IModuleDescriptor>;
}

export interface IWebresourceDependencyDescriptor {
    grouped: Array<IChunkDescriptor>;
    resources: Array<WebresourceKey>;
}

export interface IWebresourceDescriptor {
    webresourceKey: WebresourceKey;
    css: IWebresourceDependencyDescriptor;
    js: IWebresourceDependencyDescriptor;
}

export interface IChunkDependencyDescriptor {
    name: string;
    result: Array<IWebresourceDescriptor>;
}

export interface IAsyncChunkDependencyDescriptor extends IChunkDependencyDescriptor {
    async: Array<IChunkDependencyDescriptor>;
}

export interface IWebresourceSizes {
    css: number;
    js: number;
    total: number;
}

export interface ISizesReportDescriptor {
    direct: ISizesDescriptor;
    all: ISizesDescriptor;
}

interface IBaseExternalSizesReport {
    name: string;
    externalSize: IExternalSizeDescriptor;
    directSize: IExternalSizeDescriptor;
    indirectSize: IExternalSizeDescriptor;
    uniqueSize: IExternalSizeDescriptor;
}

export interface IExternalSizesReport extends IBaseExternalSizesReport {
    async: Array<IBaseExternalSizesReport>;
}

export enum WebresourceTypes {
    regular = 'regular',
    provider = 'provider',
    pure = 'pure',
    broken = 'broken'
}
