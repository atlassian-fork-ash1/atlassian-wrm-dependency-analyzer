import React from 'react';

import Treemap, { INodeDescriptor } from '../Treemap';

import { IWebresourceDescriptor } from '../types';

import { getWebresourceSize } from '../../utils/main';

interface IWebresourceDependencyGridProps {
    name: string;
    result: Array<IWebresourceDescriptor>;
    visible?: boolean;
    heightRatio?: number;
}

const WebresourceDependencyGrid = ({name, result, visible = true, heightRatio}: IWebresourceDependencyGridProps) => {
    if (!visible) {
        return null;
    }

    const rootNode = {
        name,
        value: 0
    };

    const transformWebresourceToTreemapNode = (webresource: IWebresourceDescriptor): INodeDescriptor => {
        const sizes = getWebresourceSize(webresource);
        let children: Array<INodeDescriptor> | undefined;

        rootNode.value += sizes.js;

        if (webresource.js.grouped.length > 1) {
            children = webresource.js.grouped.map((resource) => ({
                name: resource.webresourceKey,
                value: resource.size
            }));
        } else {
            sizes.js += webresource.js.grouped.reduce((acc, resource) => acc + resource.size, 0);
        }

        return {
            children,
            value: sizes.js,
            name: webresource.webresourceKey
        };
    };

    const entryPointNodes = result.map(transformWebresourceToTreemapNode);
    
    return (
        <Treemap
            id={name}
            heightRatio={heightRatio}
            data={{
                ...rootNode,
                children: entryPointNodes
            }}
            />
    );
}

export default WebresourceDependencyGrid;