import React, { Component } from 'react';
import get from 'lodash/get';

import { getChunkByIdx, getExternalSizesReport, getDependenciesOfDependencies } from '../../utils/data';

import Breadcrumbs from '../Breadcrumbs';
import Size from '../Size';
import FixedContainer from '../FixedContainer';
import DependenciesList from '../DependenciesList';

interface IAsyncChunkProps {
    entryPointIdx: number;
    chunkIdx: number;
}

export default class AsyncChunk extends Component<IAsyncChunkProps> {
    dependenciesTableHead = [
        {
            key: 'name',
            content: 'Name',
            isSortable: true,
        },
    ];

    render() {
        const { entryPointIdx, chunkIdx } = this.props;
        const externalSizesReport = getExternalSizesReport();
        const chunk = getChunkByIdx(entryPointIdx, chunkIdx);
        const directSize = get(externalSizesReport, [entryPointIdx, 'async', chunkIdx, 'directSize', 'total']);
        const externalSize = get(externalSizesReport, [entryPointIdx, 'async', chunkIdx, 'externalSize', 'total']);

        const dependencies = chunk.result.map(dependency => dependency.webresourceKey);
        const dependenciesOfDependencies = getDependenciesOfDependencies(entryPointIdx, chunkIdx);

        return (
            <div className="async-chunk">
                <FixedContainer>
                    <div className="container">
                        <Breadcrumbs />
                        <h2>Async chunk:</h2>
                        <h1>{chunk.name}</h1>
                    </div>
                </FixedContainer>
                <div className="container">
                    <div className="summary__info">
                        <ul>
                            <li>
                                <i>Size without dependencies:</i> <Size content={directSize} />
                            </li>
                            <li>
                                <i>Size including all dependencies::</i> <Size content={externalSize} />
                            </li>
                        </ul>
                    </div>
                    <h2>
                        Dependencies of <i>{chunk.name}</i>
                    </h2>
                    <DependenciesList asyncChunkIdx={chunkIdx} entryPointIdx={entryPointIdx} content={dependencies} />

                    <h2>
                        Dependencies of dependencies for <i>{chunk.name}</i>
                    </h2>
                    <DependenciesList
                        asyncChunkIdx={chunkIdx}
                        entryPointIdx={entryPointIdx}
                        content={dependenciesOfDependencies}
                    />
                </div>
            </div>
        );
    }
}
