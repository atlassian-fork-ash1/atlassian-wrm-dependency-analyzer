import React, { Component } from 'react'
import Tabs from '@atlaskit/tabs';

import { getEntryPointByIdx } from '../../utils/data';
import { getResourceUsageMap } from '../../utils/main';

import Breadcrumbs from '../Breadcrumbs';
import FixedContainer from '../FixedContainer/index';
import WebresourceDependencyGrid from '../WebresourceDependencyGrid';
import ExternalDependencies from './ExternalDependencies';
import AtomicSizes from './AtomicSizes';
import AsyncChunks from './AsyncChunks';

import './style.less'

interface IEntryProps {
    entryPointIdx: number;
}

class Entry extends Component<IEntryProps> {
    render() {
        const { entryPointIdx } = this.props;
        const entryPoint = getEntryPointByIdx(entryPointIdx);

        const resourcesUsageMap = getResourceUsageMap(entryPoint.result);

        return (
            <div className="entry">
                <FixedContainer>
                    <div className="container">
                        <Breadcrumbs />
                    </div>
                </FixedContainer>
                <div className="container">
                    <Tabs
                        tabs={[
                            {
                                label: 'External dependencies',
                                content: <ExternalDependencies
                                    entryPointIdx={entryPointIdx}
                                    resourcesUsageMap={resourcesUsageMap}
                                    />
                            },
                            {
                                label: "Async chunks",
                                content: <AsyncChunks entryPointIdx={entryPointIdx} />
                            },
                            {
                                label: 'Atomic sizes',
                                content: <AtomicSizes 
                                    entryPointIdx={entryPointIdx}
                                    resourcesUsageMap={resourcesUsageMap}
                                    />
                            },
                            {
                                label: 'Dependencies grid',
                                content: <WebresourceDependencyGrid
                                    name={name}
                                    result={entryPoint.result}
                                    heightRatio={1.5}
                                    />
                            }
                        ]}
                        />
                    
                </div>
            </div>
        );
    }
}

export default Entry;