import React, { Component } from 'react'
import { Link } from 'react-router-dom';

import { getSizesReport, getEntryPointByIdx, getWebresourceSummary } from '../../../utils/data';
import { IResourcesUsage, getResourceUsageInfo } from '../../../utils/main';
import Size from '../../Size';
import Table from '../../Table/index';

import './style.less';

interface IExternalDependenciesProps {
    entryPointIdx: number;
    resourcesUsageMap: IResourcesUsage;
}

class ExternalDependencies extends Component<IExternalDependenciesProps> {
    tableHead = [
        {
            key: 'webresource',
            content: 'Webresource',
            isSortable: true
        },
        {
            key: 'type',
            content: 'Type',
            isSortable: true
        },
        {
            key: 'total',
            content: 'Bundled size',
            isSortable: true
        },
        {
            key: 'uniq',
            content: 'Not shared with other',
            isSortable: true
        },
        {
            key: 'shared',
            content: 'Shared with other',
            isSortable: true
        },
        {
            key: 'rationNumber',
            content: ''
        },
        {
            key: 'ratio',
            content: 'Ratio',
            isSortable: true
        }
    ]

    render() {
        const { entryPointIdx, resourcesUsageMap } = this.props;
        const entryPoint = getEntryPointByIdx(entryPointIdx);
        const sizesReport = getSizesReport();

        const externalDependenciesRows: any = entryPoint.result.map(webresource => {
            const totalSize = sizesReport.direct[webresource.webresourceKey];
            const usage = getResourceUsageInfo(resourcesUsageMap, webresource);
            const resourcesCount = webresource.js.resources.length;

            const { type: resourceType } = getWebresourceSummary(entryPoint, webresource.webresourceKey);

            return [
                {
                    key: 'webresource',
                    content: <Link to={`/entry/${entryPointIdx}/resource/${webresource.webresourceKey}`}>{webresource.webresourceKey}</Link>,
                    value: webresource.webresourceKey
                },
                {
                    key: 'type',
                    content: <div className={`external-dependencies__resource-type external-dependencies__resource-type_${resourceType}`}>{resourceType}</div>,
                    value: resourceType
                },
                {
                    key: 'total',
                    content: <Size content={totalSize.total} />,
                    value: totalSize.total
                },
                {
                    key: 'uniq',
                    content: <Size content={usage.uniqSize} />,
                    value: usage.uniqSize
                },
                {
                    key: 'shared',
                    content: <Size content={usage.sharedSize} />,
                    value: usage.sharedSize
                },
                {
                    key: 'ratioNumber',
                    content: `${usage.uniqCount} / ${resourcesCount}`
                },
                {
                    key: 'ratio',
                    content: (
                        <div className="ratio-bar">
                            <div className="ratio-bar__value" style={{width: `${(usage.uniqCount / resourcesCount) * 100}%`}}/>
                        </div>
                    ),
                    value: resourcesCount === 0 ? 0 : usage.uniqCount / webresource.js.resources.length
                }
            ];
        });

        return (
            <Table
                label="External dependencies"
                filterKey="webresource"
                head={this.tableHead}
                rows={externalDependenciesRows}
                />
        )
    }
}

export default ExternalDependencies;