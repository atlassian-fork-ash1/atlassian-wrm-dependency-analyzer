import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LinkIcon from '@atlaskit/icon/glyph/link';

import { getEntryPointByIdx } from '../../../utils/data';
import { IChunkDependencyDescriptor } from '../../types';
import DependenciesList from '../../DependenciesList';

import './style.less';

interface IAsyncChunksProps {
    entryPointIdx: number;
}

export default class AsyncChunks extends Component<IAsyncChunksProps> {
    render() {
        const { entryPointIdx } = this.props;
        const entryPoint = getEntryPointByIdx(entryPointIdx);

        if (!entryPoint.async || !entryPoint.async.length) {
            return <div className="async-chunks__empty">There is no async chunks in {entryPoint.name}</div>;
        }

        return (
            <div className="async-chunks">
                <h2>Async chunks</h2>
                {entryPoint.async.map(this.renderAsyncChunk)}
            </div>
        );
    }

    renderAsyncChunk = (chunk: IChunkDependencyDescriptor, chunkIdx: number) => {
        const { entryPointIdx } = this.props;
        const dependencies = chunk.result.map(dependency => dependency.webresourceKey);

        return (
            <div key={chunkIdx} className="async-chunks__chunk">
                <h3>
                    {chunk.name}
                    <Link className="async-chunks__link" to={`/entry/${entryPointIdx}/chunk/${chunkIdx}`}>
                        <LinkIcon label={`Go to ${chunk.name}`} />
                    </Link>
                </h3>
                {dependencies.length ? (
                    <DependenciesList entryPointIdx={entryPointIdx} asyncChunkIdx={chunkIdx} content={dependencies} />
                ) : (
                    <div className="async-chunks__empty">No direct dependencies in {chunk.name} async chunk</div>
                )}
            </div>
        );
    };
}
