import React, { Component } from 'react';

import Breadcrumbs from '../Breadcrumbs';
import EntryItem from './Item/index';

import { IAsyncChunkDependencyDescriptor } from '../types';

import './style.less';

interface IEntriesListProps {
    content: Array<IAsyncChunkDependencyDescriptor>;
}

export default class EntriesList extends Component<IEntriesListProps> {
    render() {
        const { content } = this.props;

        return (
            <div className="entries-list">
                <div className="container">
                    <Breadcrumbs />

                    {content.map((entry, idx) => (
                        <div className="entries-list__item" key={idx}>
                            <EntryItem name={entry.name} idx={idx} />
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}
