import React, { Component } from 'react';

import DependenciesList from '../../DependenciesList/index';
import Size from '../../Size';

import { getEntryPointByIdx, getWebresourceSummary } from '../../../utils/data';
import { WebresourceKey } from '../../types';

import './style.less';

interface IResourceSummaryProps {
    entryPointIdx: number;
    resourceKey: WebresourceKey;
}

class ResourceSummary extends Component<IResourceSummaryProps> {
    getSizesTableRows() {
        return [
            [
                {
                    key: 'hey',
                    content: 'Hey'
                },
                {
                    key: 'hey2',
                    content: 'hey2'
                },
                {
                    key: 'hey2',
                    content: 'hey2'
                }
            ]
        ]
    }

    render() {
        const { entryPointIdx, resourceKey } = this.props;
        const entryPoint = getEntryPointByIdx(entryPointIdx);
        const {
            externalSize,
            uniqSize,
            uniqDependencies,
            uniqDependenciesSize,
            type: resourceType
        } = getWebresourceSummary(entryPoint, resourceKey);

        const sizeInBundle = uniqSize + uniqDependenciesSize;

        return (
            <div>
                <div className={`summary__resource-type summary__resource-type_${resourceType}`}>{resourceType}</div>
                <div className="summary__info">
                    <ul>
                        <li><i>Size including all dependencies:</i> <Size content={externalSize} /></li>
                        <li><i>Size without dependencies:</i> <Size content={uniqSize} /></li>
                        <li><i>Size added to current entrypoint (excluding shared dependencies):</i> <Size content={sizeInBundle} /></li>
                    </ul>
                </div>

                {uniqDependencies.length ? (
                    <>
                        <h2>Uniq dependencies</h2>
                        <DependenciesList content={uniqDependencies} />
                    </>
                ) : null}
            </div>
        );
    }
}

export default ResourceSummary;