import React, { Component, ReactElement } from 'react';
import { Link } from 'react-router-dom';
import LinkIcon from '@atlaskit/icon/glyph/link';

import Table from '../../Table';
import DependenciesList from '../../DependenciesList';
import VennDiagram from '../../VennDiagram';
import Spoiler from '../../Spoiler';
import Size from '../../Size';

import { getEntryPointByIdx, getIntersections, getSizesReport } from '../../../utils/data';

import { WebresourceKey } from '../../types';

import './style.less';

interface IResourceIntersesctionsProps {
    entryPointIdx: number;
    resourceKey: WebresourceKey;
    label: ReactElement | string;
}

export default class ResourceIntersesctions extends Component<IResourceIntersesctionsProps> {
    tableHead = [
        {
            key: 'webresourceKey',
            content: 'Webresource key',
            isSortable: true,
        },
        {
            key: 'count',
            content: 'Percent of shared code',
            isSortable: true,
        },
        {
            key: 'size',
            content: 'Shared size',
            isSortable: true,
        },
    ];

    render() {
        const { entryPointIdx, resourceKey, label } = this.props;
        const intersectionRows = this.getIntersectionsRows(entryPointIdx, resourceKey);

        return <Table label={label} head={this.tableHead} rows={intersectionRows} />;
    }

    getIntersectionsRows(entryPointIdx: number, resourceName: WebresourceKey) {
        const entryPoint = getEntryPointByIdx(entryPointIdx);
        const sizeReport = getSizesReport();
        const intersections = getIntersections(entryPoint, resourceName);
        const resourceSize = sizeReport.direct[resourceName].total;

        return intersections.map(intersection => {
            const sharedCodePercent = (intersection.size / resourceSize) * 100;
            const [, webresourceWithSameDependencies] = intersection.webresourceKeys;
            const defaultSet = [
                {
                    sets: intersection.webresourceKeys,
                    label: `${intersection.size / 1024}kb`,
                    figure: intersection.size,
                    size: intersection.size,
                },
            ];

            const sets = intersection.webresourceKeys.reduce((sets, webresource) => {
                sets.push({
                    sets: [webresource],
                    label: webresource,
                    figure: sizeReport.direct[webresource].total,
                    size: sizeReport.direct[webresource].total,
                });

                return sets;
            }, defaultSet);

            return [
                {
                    key: 'webresourceKey',
                    content: (
                        <div className="intersection">
                            {webresourceWithSameDependencies}
                            <Link
                                className="intersection__link-icon"
                                to={`/entry/${entryPointIdx}/resource/${webresourceWithSameDependencies}`}
                            >
                                <LinkIcon label={`Go to ${webresourceWithSameDependencies}`} />
                            </Link>
                            <Spoiler label="Common dependencies">
                                <>
                                    <VennDiagram
                                        id={intersection.webresourceKeys.join('-')}
                                        sets={sets}
                                        width={400}
                                        height={400}
                                    />
                                    <DependenciesList content={intersection.dependencies} />
                                    <div className="separator" />
                                </>
                            </Spoiler>
                        </div>
                    ),
                    value: webresourceWithSameDependencies,
                },
                {
                    key: 'count',
                    content: `${sharedCodePercent.toFixed(1)}%`,
                    value: sharedCodePercent,
                },
                {
                    key: 'size',
                    content: <Size content={intersection.size} />,
                    value: intersection.size,
                },
            ];
        });
    }
}
