import React, { Component } from 'react';

import FixedContainer from '../FixedContainer';
import Breadcrumbs from '../Breadcrumbs';
import DependenciesList from '../DependenciesList';
import ResourceSummary from './Summary';
import ResourceIntersections from './Intersections';

import { WebresourceKey } from '../types';
import { getEntryPointByIdx, getWebresourceByKey, getChunkByIdx } from '../../utils/data';

import './style.less';

interface IWebresourceProps {
    entryPointIdx: number;
    resourceName: WebresourceKey;
    chunkIdx?: number;
}

export default class Webresource extends Component<IWebresourceProps> {
    render() {
        const { entryPointIdx, chunkIdx, resourceName } = this.props;

        const entryPoint = getEntryPointByIdx(entryPointIdx);
        const chunk = chunkIdx ? getChunkByIdx(entryPointIdx, chunkIdx) : null;
        const webresource = getWebresourceByKey(chunk || entryPoint, resourceName);

        if (!webresource) {
            return <div className="container">Can not find webresource {resourceName}</div>;
        }

        return (
            <div className="container">
                <FixedContainer>
                    <div className="container">
                        <Breadcrumbs />
                        <h2>Webresource:</h2>
                        <h1>{resourceName}</h1>
                        {chunk ? (
                            <div className="webresource__chunk-name">
                                in context of <i>{chunk.name}</i> async chunk
                            </div>
                        ) : null}
                    </div>
                </FixedContainer>

                <ResourceSummary entryPointIdx={entryPointIdx} resourceKey={resourceName} />

                <h2>
                    Dependencies of <i>{resourceName}</i>
                </h2>
                <DependenciesList entryPointIdx={chunkIdx ? null : entryPointIdx} content={webresource.js.resources} />

                <ResourceIntersections
                    label={
                        <>
                            Intersections with other external dependencies of <i>{entryPoint.name}</i>
                        </>
                    }
                    entryPointIdx={entryPointIdx}
                    resourceKey={resourceName}
                />
            </div>
        );
    }
}
