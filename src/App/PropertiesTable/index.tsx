import React, {Component, ReactElement} from 'react';

import './style.less';

export interface IPropertiesTableRow {
    name: string | number | ReactElement;
    value?: any;
    values?: Array<any>;
}

interface IPropertiesTableProps {
    content?: Array<IPropertiesTableRow>;
    valueWrapper?: (val: any) => void
}

export default class PropertiesTable extends Component<IPropertiesTableProps> {
    render() {
        const {content, valueWrapper} = this.props;

        if (!content || !content.length) {
            return null;
        }

        return (
            <div className="properties-table">
                {content.map((row, rowIdx) => {
                    const {name, value, values = []} = row;

                    if (value) {
                        
                    }

                    const valuesToDisplay = value ? [value, ...values] : values;

                    return (
                        <div className="properties-table__row" key={rowIdx}>
                            <div className="properties-table__name">
                                {name}
                            </div>
                            {valuesToDisplay.map((val, cellIdx) => (
                                <div className="properties-table__cell" key={cellIdx}>
                                    {valueWrapper ? valueWrapper(val) : val}
                                </div>
                            ))}
                        </div>
                    );
                })}
            </div>
        );
    }
}