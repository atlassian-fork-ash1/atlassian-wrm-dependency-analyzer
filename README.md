### WRM Dependencies analyzer

Visualize dependencies and async chunks of WRM Plugin (not necessary). 

##### Run
```
yarn install
yarn start
```


#### Usage
```
wrm-dependency-analyzer ./report.json
```